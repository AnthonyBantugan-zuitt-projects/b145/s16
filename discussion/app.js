console.log('Hello from JS')

//[SECTION 1] Assignment Operators

//1.Basic Assignment Opertor(=)
//this allows us to add the value of the right operand to a variable and assigns the result to the variable.
let assignmentNumber = 5;

let message = 'This is the message';

//2. Addition assignment operator (+=)
//The addition assignment operator add the value of the right operand to a variable and assigns the result to the variable.
//assignmentNumber = assignmentNumber + 2; //7

//shorthanded version of the statement above
assignmentNumber += 2;
console.log("result of the operation: " + assignmentNumber);

    //[SECTION 1 SUB 2] Arithmetic Operators
    //(+, -, *, /)
//3. Subtraction/Multiplication/Division Assignment Operator (-=/ *=/ /=)
assignmentNumber -= 3;
console.log("result of the operation: " + assignmentNumber); //4 

//RECAP Assingment Operators

// let value = 8;

//addition assignment (+=)
//value += 15;

//subtraction assignment (-=)
//value -= 5; //3

//multiplication assignment (*=)
// value *= 2; //16

//division assignment (/=)
// value /= 2;

// console.log(value);
// -----------------------------------------------------------------


// [SECTION] Arithmetic Operators

let x = 15;
let y = 10;

//addition (+)
let sum = x + y
console.log(sum); 

//subtraction (-)
let difference = x - y;
console.log(difference); //5

//multiplication (*)
let product = x * y;
console.log(product); //150

//division (/)
let quotient = x/y;
console.log(quotient); //1.5

//remainder between the 2 values (Modulus '%')
let remainder = x % y;
console.log(remainder); //5
// ----------------------------------------------------------------

//[SUBSECTION] Multiple Operators and Parentheses

//When multiple operators are applied in a single statement, it follows the PEMDAS rule.(Parenthesis, Exponent, Multiplication, Division, Addition, and Subtraction)

 let mdas = 1 + 2 - 3 * 4 / 5;
 console.log(mdas);

 //The operation were done in the following order to get the final answer.
    //1. 3 * 4 = 12
    //2. 12 / 5 = 2.4
    //3. 1 + 2 = 3
    //4. 3 - 2.4 = 0.6

// NOTE: The order of operations can be changed by adding parenthesis to the logic
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

//By adding parenthesis '()', the order of the operations are changed prioritizing  the operations enclosed within parenthesis.
// this operation was done with the following order:
    //1. 4 / 5 = 0.8
    //2. 2 - 3 = -1
    //3. -1 * 0.8 = -0.8
    //4. 1 + - 0.8 = 0.2
// ----------------------------------------------------------------

// [Section] Increment or Decrement
let z = 1;

// Pre and Post Increment/Decrement
//Increment (++)

// Pre-increment (syntax: ++variable)

// the value of "z" is added by a value of 1 before returning the value and storing it inside a new variable: "preIncrement"
let preIncrement = ++z; //1 + 1
console.log(preIncrement) //result of the pre-increment

// we can see here that the value of "z" was also increased even though we did NOT implicitly specified any variablereassignment.
console.log(z);

// Post-increment (syntax: variable++)
let postIncrement = z++;

// the value of "z" is returned and stored inside the variable called "postIncrement". The value of z is at 2 before it was incremented
console.log(postIncrement); //2
// z + 1
console.log(z); //3 
// 1 + z VS z + 1

// Decerement (--)
// Pre-decrement (syntax: --variable)
// the value of Z starts with 3 before it was decremented
let preDecrement = --z; //3
console.log(preDecrement);

// Post-decrement (syntax: variable--)
let postDecrement = z--;
// the value of "z" is returned and stored inside a variable before it will be decremented.
console.log(postDecrement);
console.log(z);

let bagongValue = 3;
// let newValue = ++bagongValue;
// console.log('new value using pre-increment: '+ bagongValue); //4
let newValue = bagongValue++;
console.log('new value using pre-increment: '+ newValue); //4
console.log(bagongValue);

//real life situations in programming where we use this type of operation:
    //1. queues
    //2. creating loop conditions
// ----------------------------------------------------------------


//[SECTION:] TYPE COERCION

let numA = 6;
let numB = '6';

// lets check the data types of the values above
// 'typeof' expression -> will allow us to identify the data type of a certain value/component.
console.log(typeof numA); //number
console.log(typeof numB); //string
// number + string //the number data type ws converted into a string to perform concatenation instead of addition.

let coercion = numA + numB;
console.log(typeof coercion); //concatenation



//Adding number and boolean=====================
let expressionC = 10 + true;
console.log(expressionC);

let a = true;
console.log(typeof a); //boolean (TRUE = 1, FALSE = 0)
let b = 10;
console.log(typeof b); //number
// NOTE: the boolean value of "true" is also associated with the value of 1.

let expressionD = true + true + true; (1 + 1 + 1)
console.log(expressionD); //3

let expressionE = 10 + false;
console.log(expressionE);

// the boolean value of "false" is associated with a value of 0.

let expressionF = true + false; //1 + 0 = 1
console.log(expressionF); //1



// Number with a Null value====================
let expressionG = 8 + null;
console.log(expressionG); //8 (string, number, boolean)
// 8 + 0 = 8

let d = null;
console.log(typeof d); //object

// Conversion Rules:
    // 1.If atleast one operand is an object, it will be converted into a primitive value/data type. 
    // 2.After conversion, if atleast one operand is a string data type, the 2nd operand is converted into another string to perform concatenation.
    // 3.In other cases where both operands are converted to numbers then an arithmetic operation is executed.

// the object data type which is "null" operand was converted into a primitive data type.


// String with a NULL data type ================
let expressionH = "Batch145" + null; //"null" will be converted into a primitive data type. null -> "null"
// "Batch145" + "null" = "Batch145null"

console.log(expressionH); //Batch145null
// 1. Batch145 -> string data type



// Number with undefined====================
expressionH = 9 + undefined; //NaN -> Not a Number
console.log(expressionH); 
let e = undefined;
console.log(typeof e);

// 1. "undefined" was converted into a number data type NaN
// 2. 9 + NaN = NaN
// =========================================================



// [SECTION] Comparison Operators
let name = 'Juan';

    // [SUB SECTION] EQUALITY OPERATORS (==)
    // attempts to CONVERT AND COMPARE operand with 2 different data types.
    // -> returns a boolean value
    console.log(1 == 1); //true
    console.log(1 == 2); //false
    console.log(1 == "1"); //true
    console.log(1 == true); //true
    console.log(1 == false); //true
    console.log(name == 'Juan'); //true
    console.log('Juan' == 'juan'); //false (CASE SENSITIVE)
    // console.log('Juan' == Juan); //Error -> because the variable was not yet declared.
// -----------------------------------------------------------

    // [SUB SECTION] INEQUALITY OPERATOR (!=)

    // ->checks wether the operands are NOT EQUAL /HAVE DIFFERENT VALUES.
    
    console.log(1 != 1); //false
    console.log(1 != 2); //true
    console.log(1 != '1'); //false
    console.log(0 != false); //false
    let juan = 'juan';
    console.log('juan' != juan); //false
// ---------------------------------------------------------

    // [SUBSECTION] "STRICT" EQUALITY OPERATORS
    // -> checks wether the operands are equal or have the same value
    // -> Also compares if the data types are the same.

    console.log(1 === 1); //true
    console.log(1 === '1'); //false
    console.log(0 === false); //false
    // they have different data types hence, false.
// ----------------------------------------------------------

    // [SUBSECTION] "STRICT" INEQUALITY OPERATOR (!==)

    // this will check if the operands are NOT EQUAL?HAVE DIFFERENT values/content.
    // chck both values and data types of both component/operands.

    console.log(1 !== 1); //false
    console.log(1 !== 2); //true
    console.log(1 !== '1'); //true
    console.log(0 !== false); //true

// Developer's tip: upon creating conditions or statements it is strongly recommendedto use "Strict" equality operators over "loose" equality operators because it will be easier for us to predetermine outcomes and results in any given scenario.
// =====================================================


// [SECTION] RELATIONAL OPERAND
let priceA = 1800;
let priceB = 1450;

// lesser than operation //false
console.log(priceA < priceB);
// greater than operator //true
console.log(priceA > priceB);

let expressionI = 150 <= 150;
console.log(expressionI);
// ===========================================================

// [SECTION] LOGICAL OPERATOR
//Developer's Tip: When writing down/selecting variables name that would describe/contain a boolean value. it is writing convention for developers to add a prefix of "is" or "are" together with the variable name to form a variable similar on how to answer a simple yes or no question.

// is + Single = true;
// are + Taken = false;
// are + Lisening = true;

isLegalAge = true;
isRegistered = false;

// for the person to be able to vote, oth requirements has to be met.

// we need to use Proper Logical operator
// AND (&& Double Ampersand) all criteria has to be MET.
let isAllowedToVote = isLegalAge && isRegistered;
console.log('Is the person allowed to vote? ' + isAllowedToVote);

// OR (|| Double Pipe) - atleast 1 criteria has to be MET. in order to pass.
let isALlowedForVaccination = isLegalAge || isRegistered;
console.log('Did the person pass? ' + isALlowedForVaccination);

// NOT OPERATOR (! - Excalamation Point)
    // This will convert/return the opposite value.
    let isTaken = true;
    let isTalented = false;

    console.log(!isTaken); //false
    console.log(!isTalented); //true



    
