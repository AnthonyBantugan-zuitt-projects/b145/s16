console.log('Hello World');
// =================================


// 3. Create two variables that will store two different number values and create several variables that will store the sum, difference, product and quotient of the two numbers. Print the results in the console
// 4. Using comparison operators print out a string in the console that will return a message if the sum is greater than the difference.
// 5. Using comparison and logical operators print out a string in the console that will return a message if the product and quotient are both positive numbers.
// 6. Using comparison and logical operators print out a string in the console that will return a message if one of the results is a negative number.
// 7. Using comparison and logical operators print out a string in the console that will return a message if one of the results is zero.

let varA = 7;
let varB = 23;

// INSTRUCTION #3============================
let sum = varA + varB;
console.log('The sum of the two numbers is: ' + sum);

let difference = varA - varB;
console.log('The difference of the two numbers is: ' + difference);

let product = varA * varB;
console.log('The product of the two numbers is: ' + product);

let quotient = varA / varB;
console.log('The quotient of the two numbers is: ' + quotient);

// INSTRUCTION #4=============================
let questionFour = sum >= difference;
console.log('The sum is greater than the difference: ' +  questionFour);

// INSTRUCTORS ANSWER #4
// let isGreater = sum > difference;
// console.log('The sum is greater than the difference: ' +  isGreater); 



// INSTRUCTION #5=============================
let isPositive = (product > 0) && (quotient > 0);
console.log('The product and quotient are positive numbers: ' + isPositive);

// INSTRUCTORS ANSWER #5
// const arePositiveNumbers = product > 0 && quotient > 0;
// console.log('The product and quotient are positive numbers: ' + arePositiveNumbers);


let isSumNegative = sum < 0;
let isDifferenceNegative = difference < 0;
let isProductNegative = product < 0;
let isQuotientNegative = quotient < 0;

// INSTRUCTION #6=============================
let isOneNegative = isSumNegative || isDifferenceNegative || isProductNegative || isQuotientNegative;
console.log('One of the results is negative: ' + isOneNegative);

// INSTRUCTORS ANSWER #6
// let isNegative = sum || < 0 difference || < 0 product || < 0 quotient < 0;
// console.log('One of the results is negative: ' + isNegative);

// INSTRUCTION #7=============================
let isZero = (sum, difference, product, quotient >= 0);
console.log('All the results are not equal to zero: ' + isZero);

// INSTRUCTORS ANSWER
// let notZero = sum != 0 || difference != 0 || product != 0 || quotient != 0;
// console.log('All the results are not equal to zero: ' + notZero);